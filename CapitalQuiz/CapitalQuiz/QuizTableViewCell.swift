	//
//  QuizCellViewTableViewCell.swift
//  CapitalQuiz
//
//  Created by JASI on 11/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import UIKit

class QuizTableViewCell: UITableViewCell {

    @IBOutlet weak var quesTxt : UILabel!
    @IBOutlet weak var hintTxt : UILabel!
    @IBOutlet weak var option1 : UIButton!
    @IBOutlet weak var option2 : UIButton!
    @IBOutlet weak var option3 : UIButton!
    @IBOutlet weak var option4 : UIButton!
            
    func updateViews( ques : Question ) {
        quesTxt.text = ques.questionTxt
        hintTxt.text = ques.hintTxt
        option1.setTitle(ques.option1, for: .normal)
        option2.setTitle(ques.option2, for: .normal)
        option3.setTitle(ques.option3, for: .normal)
        option4.setTitle(ques.option4, for: .normal)
    }

}
