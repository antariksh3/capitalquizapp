//
//  Question.swift
//  CapitalQuiz
//
//  Created by JASI on 10/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import Foundation


class Question {
    
    var questionId : Int
    var questionTxt : String
    var hintTxt : String
    
    var option1 : String
    var option2 : String
    var option3 : String
    var option4 : String
    
    var correctOption : String
    var selectedOption : Int
    
    
    init( quesId : Int, quesTxt : String , hint : String , op1 : String , op2 : String , op3 : String , op4 : String , correctOp : String , selectedOp : Int ) {
        self.questionId = quesId
        self.questionTxt = quesTxt
        self.hintTxt = hint
        self.option1 = op1
        self.option2 = op2
        self.option3 = op3
        self.option4 = op4
        self.correctOption = correctOp
        self.selectedOption = selectedOp
        
    }
    
    
    
}
