//
//  ResultVC.swift
//  CapitalQuiz
//
//  Created by JASI on 10/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import UIKit

extension UIViewController {
    func performSegueToReturnBack()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

class ResultVC: UIViewController {

    var finalScore = Int()

    @IBOutlet weak var scoreBoard: UILabel!
    @IBOutlet weak var scoreMessage: UILabel!
    @IBOutlet weak var btnRetry: CustomButtonView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("finallll",finalScore)
        scoreBoard.text = String(finalScore) + "/5"
        
        btnRetry.isHidden = true
        
        if finalScore <= 2 {
            btnRetry.isHidden = false
            scoreMessage.text = "Please Try Again!"
        } else if finalScore == 3 {
            scoreMessage.text = "Good Job!"
        } else if finalScore == 4 {
            scoreMessage.text = "Excellent Work!"
        } else if finalScore == 5 {
            scoreMessage.text = "You Are A Genius"
        }
        
    }

    @IBAction func exitTapped(_ sender: Any) {
        exit(0)
    }
    @IBAction func retryTapped(_ sender: Any) {
        
        //back
        self.dismiss(animated: true, completion: nil)
        //performSegueToReturnBack()
        //root
        //_ = navigationController?.popToRootViewController(animated: true)

    }
    
    
    
}
