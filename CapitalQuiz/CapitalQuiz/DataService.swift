//
//  DataService.swift
//  CapitalQuiz
//
//  Created by 			     on 11/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import Foundation

class DataService {
    
    static var instance = DataService()
    var quizQuestions : [Question] = []
    var questions = [
        Question(quesId: 1, quesTxt: "What's the name of the Capital of Spain?", hint: "It is a landlocked City in the Center of Spain", op1: "Barcelona", op2: "Madrid", op3: "Seville", op4: "Betis", correctOp: "Madrid", selectedOp: 0),
        Question(quesId: 2, quesTxt: "What's the name of the City having Statue Of Liberty", hint: "It is the most happening City of USA", op1: "Los Angeles", op2: "Washington DC", op3: "New York", op4: "New Jersey", correctOp: "New York", selectedOp: 0),
        Question(quesId: 3, quesTxt: "What's the name of the City having the Eiffel Tower", hint: "It's the Capital of France", op1: "Paris", op2: "Liones", op3: "Bordeux", op4: "Nates", correctOp: "Paris", selectedOp: 0),
        Question(quesId: 4, quesTxt: "The World's first planned City", hint: "This Indian City is also called the City Beautiful", op1: "Chandigarh", op2: "Mumbai", op3: "Kolkata", op4: "New Delhi", correctOp: "Chandigarh", selectedOp: 0),
        Question(quesId: 5, quesTxt: "The Capital of the most Western European Country", hint: "The most Western European Country is Portugal", op1: "Porto", op2: "Braga", op3: "Coimbra", op4: "Lisbon", correctOp: "Lisbon", selectedOp: 0),
        Question(quesId: 6, quesTxt: "The Capital of Australia", hint: "It is Australia's largest inland city", op1: "Sydney", op2: "Canberra", op3: "Melbourne", op4: "Perth", correctOp: "Canberra", selectedOp: 0),
        Question(quesId: 7, quesTxt: "The World's Most Populated City", hint: "It is NOT the China's Capital city", op1: "Beijing", op2: "Shenzhen", op3: "Hangzhou", op4: "Shanghai", correctOp: "Shanghai", selectedOp: 0),
        Question(quesId: 8, quesTxt: "The Capital of Canada", hint: "It is Located in North-Eastern Ontario", op1: "Toronto", op2: "Ottawa", op3: "Motreal", op4: "Vancouver", correctOp: "Ottawa", selectedOp: 0),
        Question(quesId: 9, quesTxt: "The Capital of Brazil", hint: "It is Australia's largest inland city", op1: "Salvador", op2: "São Paulo", op3: "Brasília", op4: "Rio de Janeiro", correctOp: "Brasília", selectedOp: 0),
        Question(quesId: 10, quesTxt: "The Capital of Italy", hint: "One of the Most Historic City which has an Ancient Colosseum", op1: "Venice", op2: "Rome", op3: "Turin", op4: "Milan", correctOp: "Rome", selectedOp: 0)
    ]
    
    init() {
        print("populate called")
        populateArray()
    }
    
    func getQuestionByNumber ( num : Int ) -> Question {
        for ques in questions {
            if ques.questionId == num {
                return ques
            }
        }
        return Question(quesId: 0, quesTxt: "", hint: "", op1: "", op2: "", op3: "", op4: "", correctOp: "", selectedOp: 0)
    }
    
    func populateArray () {
        quizQuestions = []
        while quizQuestions.count < 5 {
            print(quizQuestions)
            let number = Int.random(in: 0 ..< 10)
            var newQues = questions[number]
            print("testing ",newQues.questionId)
            if !isQuesAlreadyAdded(ques: questions[number]) {
                quizQuestions.append(questions[number])
                print("Ques added", questions[number].questionId)
            }
        }
    }
    
    func getQuestions() -> [Question] {
        
        return quizQuestions
    }
    
    func isQuesAlreadyAdded ( ques : Question ) -> Bool {
        for que in quizQuestions {
            if ques.questionId == que.questionId {
                print("Already Present",ques.questionId)
                return true
            }
        }
        print("NOT Present",ques.questionId)
        return false
    }
}
