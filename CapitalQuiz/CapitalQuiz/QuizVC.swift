//
//  StartQuizVC.swift
//  CapitalQuiz
//
//  Created by JASI on 10/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import UIKit

class QuizVC : UIViewController , UITableViewDelegate , UITableViewDataSource {

    var score : Int = 0
    var selected : Int = 0
    var parentTable : UIView!
    @IBOutlet weak var quizTableView : UITableView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var pleaseAnswer: UILabel!

    
    
    var quesArray : [Question] = []
    var correctAnswers : [String] = ["Chandigarh","Madrid","Paris","New York","Lisbon","Rome","Brasília","Ottawa","Shanghai", "Canberra"]
    var selectedAnswers : [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.quizTableView.delegate = self
        self.quizTableView.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getQuestions().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell") as? QuizTableViewCell {
            let quizz = DataService.instance.getQuestions()[indexPath.row]
            cell.updateViews(ques: quizz)
            quesArray.append(quizz)
            
            return cell
        } else {
            return QuizTableViewCell()
        }
    }
    
    @IBAction func checkAwnser ( btn : UIButton ) {
        let celll = quizTableView.backgroundView
        let superView : QuizTableViewCell = btn.superview?.superview?.superview?.superview as! QuizTableViewCell
        celll?.backgroundColor = .green
        
        //check if ans is correct
        var optionClicked = String(btn.titleLabel?.text ?? "")
        var questionStatement = String(superView.quesTxt!.text! )
        var optionCorrect = getAnswerBasedOnQuesion(question: questionStatement)
        if ( optionClicked.lowercased() == optionCorrect.lowercased() ) {
            print("CORRECT")
        } else {
            print("IN-CORRECT")
        }
        
        var currentButton : UIButton = btn
        //print("btn.super.super",btn.superview?.superview!)
        //print("btn.super.super.subviews",btn.superview?.superview?.subviews)
        let subViews : [UIView] = (btn.superview?.superview!.subviews)!
        for view in subViews {
            //print("view in loop",view)
            let btns : [UIView] = view.subviews
            for btn in btns {
                //print("btn",btn)
                btn.backgroundColor = .white
            }
        }
        btn.backgroundColor = .yellow
        
        
        
        parentTable = currentButton.superview?.superview?.superview?.superview?.superview
        print("parentTable",parentTable)
        
        var optionsSelected : Int = 0
        
        var tableCells : [UIView] = (parentTable?.subviews)!
        for cel in tableCells {
            if cel is UITableViewCell {
                //print("cel",cel)
                var inCell : [UIView] = cel.subviews
                for element in inCell {
                    //print("element",element)
                    //print("button \(count)",element)
                    var items : [UIView] = element.subviews
                    for item in items {
                        //print("item",item)
                        if item is UIStackView {
                            //print("stack found",item)
                            var inStack : [UIView] = item.subviews
                            for innerStack in inStack {
                                if innerStack is UIStackView {
                                    var buttons : [UIView] = innerStack.subviews
                                    for button in buttons {
                                        if button is UIButton {
                                            print("final button",button)
                                            if button.backgroundColor == .yellow {
                                                optionsSelected += 1
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if optionsSelected >= 5 {
            btnSubmit.isHidden = false
            pleaseAnswer.isHidden = true
        } else {
            btnSubmit.isHidden = true
        }
    }

    
    @IBAction func btnSubmitPressed(_ sender: Any) {
        var tableCells : [UIView] = (parentTable?.subviews)!
        selectedAnswers = []
        for cel in tableCells {
            if cel is UITableViewCell {
                //print("cel",cel)
                var inCell : [UIView] = cel.subviews
                for element in inCell {
                    //print("element",element)
                    //print("button \(count)",element)
                    var items : [UIView] = element.subviews
                    for item in items {
                        //print("item",item)
                        if item is UIStackView {
                            //print("stack found",item)
                            var inStack : [UIView] = item.subviews
                            for innerStack in inStack {
                                if innerStack is UIStackView {
                                    var buttons : [UIView] = innerStack.subviews
                                    for button in buttons {
                                        if button is UIButton {
                                            print("final button",button)
                                            if button.backgroundColor == .yellow {
                                                //selected += 1
                                                print("-->",button.largeContentTitle!)
                                                selectedAnswers.append(button.largeContentTitle!)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        print("Final ans",selectedAnswers)
        
        score = 0
        for eachAns in selectedAnswers {
            if correctAnswers.contains(eachAns) {
                score += 1
            }
        }
        print("Final Score",score)
        
    }
    
    func getAnswerBasedOnQuesion( question : String ) -> String {
        for que in quesArray {
            if (que.questionTxt == question) {
                return que.correctOption
            }
        }
        return ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "quizToResult") {
            let vc = segue.destination as! ResultVC
            vc.finalScore = score
        }
    }
    
    
}
